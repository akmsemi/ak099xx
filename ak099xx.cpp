#include "ak099xx.h"

// REGISTER MAP
/* AK0991x Register Addresses */
#define AK099XX_REG_WIA1               0x00 // Company Name (data=48h)
#define AK099XX_REG_WIA2               0x01 // Device Code
#define AK099XX_REG_RSV1               0x02
#define AK099XX_REG_RSV2               0x03
#define AK099XX_REG_ST1                0x10
#define AK099XX_REG_HXL                0x11
#define AK099XX_REG_HXH                0x12
#define AK099XX_REG_HYL                0x13
#define AK099XX_REG_HYH                0x14
#define AK099XX_REG_HZL                0x15
#define AK099XX_REG_HZH                0x16
#define AK099XX_REG_TMPS               0x17
#define AK099XX_REG_ST2                0x18
#define AK099XX_REG_CNTL1              0x30
#define AK099XX_REG_CNTL2              0x31
#define AK099XX_REG_CNTL3              0x32
#define AK099XX_REG_I2CDIS             0x36
#define AK099XX_REG_ASAX               0x60
#define AK099XX_REG_ASAY               0x61
#define AK099XX_REG_ASAZ               0x62

#define AK099XX_MODE_POWERDOWN         0x00 // POWER DOWN MODE
#define AK099XX_MODE_SNG_MEASURE       0x01 // SINGLE MEASUREMENT MODE
#define AK099XX_MODE_CONT1             0x02 // CONTINUOUS MEASUREMENT MODE1 10Hz
#define AK099XX_MODE_CONT2             0x04 // CONTINUOUS MEASUREMENT MODE2 20Hz
#define AK099XX_MODE_CONT3             0x06 // CONTINUOUS MEASUREMENT MODE3 50Hz
#define AK099XX_MODE_CONT4             0x08 // CONTINUOUS MEASUREMENT MODE4 100Hz
#define AK099XX_MODE_CONT5             0x0A // CONTINUOUS MEASUREMENT MODE5 200Hz
#define AK099XX_MODE_CONT6             0x0C // CONTINUOUS MEASUREMENT MODE6 1Hz
#define AK099XX_MODE_SELF_TEST         0x10 // SELF TEST MODE 
#define AK099XX_MODE_FUSE_ACCESS       0x1F // FUSED ROM ACCESS MODE

#define AK099XX_VAL_WIA1               0x48
#define AK099XX_VAL_SOFT_RESET         0x01
#define AK099XX_VAL_I2CDIS             0x1B

#define AK099XX_BIT_MASK_DRDY          0x01
#define AK099XX_BIT_MASK_DOR           0x02
#define AK099XX_BIT_MASK_HOFL          0x08
#define AK099XX_BIT_MASK_MODE          0x1F
#define AK099XX_BIT_MASK_SDR           0x40
#define AK099XX_BIT_MASK_NSF           0x60
#define AK099XX_BIT_OFFSET_SDR         6
#define AK099XX_BIT_OFFSET_NSF         5

#define AK099XX_DATA_LENGTH            9
#define AK099XX_BUF_LENGTH_ASA         3

#define AK099XX_WAIT_POWER_DOWN_US     100
#define AK099XX_WAIT_SOFT_RESET_US     1000

/*
 *  DEVICE SPECIFIC DEFINITIONS
 */

/* AK09911 */
#define AK09911_VAL_WIA2               0x05
#define AK09911_SENSITIVITY            (0.6) // uT/LSB
#define AK09911_TLIMIT_MIN_X           -30
#define AK09911_TLIMIT_MIN_Y           -30
#define AK09911_TLIMIT_MIN_Z           -400
#define AK09911_TLIMIT_MAX_X           30
#define AK09911_TLIMIT_MAX_Y           30
#define AK09911_TLIMIT_MAX_Z           -50
#define AK09911_DENOMINATOR            128.0
#define AK09911_REQUIRE_ASA            true

/* AK09912 */
#define AK09912_VAL_WIA2               0x04
#define AK09912_SENSITIVITY            (0.15)    // uT/LSB
#define AK09912_TLIMIT_MIN_X           -200
#define AK09912_TLIMIT_MIN_Y           -200
#define AK09912_TLIMIT_MIN_Z           -1600
#define AK09912_TLIMIT_MAX_X           200
#define AK09912_TLIMIT_MAX_Y           200
#define AK09912_TLIMIT_MAX_Z           -400
#define AK09912_DENOMINATOR            256.0
#define AK09912_REQUIRE_ASA            true

/* AK09915C/D */
#define AK09915_VAL_WIA2               0x10
#define AK09915_SENSITIVITY            (0.15)    // uT/LSB
#define AK09915_TLIMIT_MIN_X           -200
#define AK09915_TLIMIT_MIN_Y           -200
#define AK09915_TLIMIT_MIN_Z           -800
#define AK09915_TLIMIT_MAX_X           200
#define AK09915_TLIMIT_MAX_Y           200
#define AK09915_TLIMIT_MAX_Z           -200
#define AK09915_DENOMINATOR            256.0
#define AK09915_REQUIRE_ASA            false

/* AK09916C */
#define AK09916C_VAL_WIA2              0x09
#define AK09916C_SENSITIVITY           (0.15)    // uT/LSB
#define AK09916C_TLIMIT_MIN_X          -200
#define AK09916C_TLIMIT_MIN_Y          -200
#define AK09916C_TLIMIT_MIN_Z          -1000
#define AK09916C_TLIMIT_MAX_X          200
#define AK09916C_TLIMIT_MAX_Y          200
#define AK09916C_TLIMIT_MAX_Z          -200
#define AK09916C_DENOMINATOR           256.0
#define AK09916C_REQUIRE_ASA           false

/* AK09916D */
#define AK09916D_VAL_WIA2              0x0B
#define AK09916D_SENSITIVITY           (0.15)    // uT/LSB
#define AK09916D_TLIMIT_MIN_X          -200
#define AK09916D_TLIMIT_MIN_Y          -200
#define AK09916D_TLIMIT_MIN_Z          -1000
#define AK09916D_TLIMIT_MAX_X          200
#define AK09916D_TLIMIT_MAX_Y          200
#define AK09916D_TLIMIT_MAX_Z          -200
#define AK09916D_DENOMINATOR           256.0
#define AK09916D_REQUIRE_ASA           false

/* AK09917 */
#define AK09917_VAL_WIA2               0x0D
#define AK09917_SENSITIVITY            (0.15)    // uT/LSB
#define AK09917_TLIMIT_MIN_X           -200
#define AK09917_TLIMIT_MIN_Y           -200
#define AK09917_TLIMIT_MIN_Z           -1000
#define AK09917_TLIMIT_MAX_X           200
#define AK09917_TLIMIT_MAX_Y           200
#define AK09917_TLIMIT_MAX_Z           -150
#define AK09917_DENOMINATOR            256.0
#define AK09917_REQUIRE_ASA            false

/* AK09918C */
#define AK09918_VAL_WIA2               0x0C
#define AK09918_SENSITIVITY            (0.15)    // uT/LSB
#define AK09918_TLIMIT_MIN_X           -200
#define AK09918_TLIMIT_MIN_Y           -200
#define AK09918_TLIMIT_MIN_Z           -1000
#define AK09918_TLIMIT_MAX_X           200
#define AK09918_TLIMIT_MAX_Y           200
#define AK09918_TLIMIT_MAX_Z           -150
#define AK09918_DENOMINATOR            256.0
#define AK09918_REQUIRE_ASA            false

AK099XX::AK099XX(){
    i2c = NULL;
    spi = NULL;
    cs = NULL;
}

AK099XX::~AK099XX(){
    if(i2c) delete i2c;
    if(spi) delete spi;
    if(cs) delete cs;
}

void AK099XX::init_sub() {
    
    // software reset
    char buf = AK099XX_VAL_SOFT_RESET;
    AK099XX::write(AK099XX_REG_CNTL3, &buf, AKMECOMPASS_LEN_ONE_BYTE);
    wait_us(AK099XX_WAIT_SOFT_RESET_US);
    
    // get sensitivity data from Fused-ROM
    getSensitivityAdjustment(&sensitivityAdjustment);
    
    // set sensitivity and denominator            
    switch(deviceId)
    {    
        case AK09911:
        {
            sensitibity = AK09911_SENSITIVITY;
            denominator = AK09911_DENOMINATOR;
            isRequiredAsa = AK09911_REQUIRE_ASA;
            break;
        }
        case AK09912:
        {
            sensitibity = AK09912_SENSITIVITY;
            denominator = AK09912_DENOMINATOR;
            isRequiredAsa = AK09912_REQUIRE_ASA;
            break;
        }
        case AK09915:
        {
            sensitibity = AK09915_SENSITIVITY;
            denominator = AK09915_DENOMINATOR;
            isRequiredAsa = AK09915_REQUIRE_ASA;
            break;
        }
        case AK09916C:
        {
            sensitibity = AK09916C_SENSITIVITY;
            denominator = AK09916C_DENOMINATOR;
            isRequiredAsa = AK09916C_REQUIRE_ASA;
            break;
        }
        case AK09916D:
        {
            sensitibity = AK09916D_SENSITIVITY;
            denominator = AK09916D_DENOMINATOR;
            isRequiredAsa = AK09916D_REQUIRE_ASA;
            break;
        }
        case AK09917:
        {
            sensitibity = AK09917_SENSITIVITY;
            denominator = AK09917_DENOMINATOR;
            isRequiredAsa = AK09917_REQUIRE_ASA;
            break;
        }
        case AK09918:
        {
            sensitibity = AK09918_SENSITIVITY;
            denominator = AK09918_DENOMINATOR;
            isRequiredAsa = AK09918_REQUIRE_ASA;
            break;
        }
        default:
        {
            sensitibity = AK09918_SENSITIVITY;
            denominator = AK09918_DENOMINATOR;
            isRequiredAsa = AK09918_REQUIRE_ASA;
            break;
        }            
    }
}

void AK099XX::init(I2C *_i2c, SlaveAddress addr, DeviceId id) {
    if(i2c) delete i2c;
    if(spi) delete spi;
    if(cs) delete cs;
    i2c = _i2c;
    slaveAddress = addr;    
    deviceId = id;
    
    init_sub();
}

void AK099XX::init(SPI *_spi, DigitalOut *_cs, DeviceId id) {
    if(i2c) delete i2c;
    if(spi) delete spi;
    if(cs) delete cs;
    spi=_spi;
    cs=_cs;
    cs->write(1);
    deviceId = id;

    init_sub();

    // disable I2C
    char buf = AK099XX_VAL_I2CDIS;
    AK099XX::write(AK099XX_REG_I2CDIS, &buf, AKMECOMPASS_LEN_ONE_BYTE);
}

AkmECompass::Status AK099XX::checkConnection() {
    AkmECompass::Status status = AkmECompass::SUCCESS;
    
    // Gets the WIA1 register value.
    char wia1Value = 0;
    if ((status=AK099XX::read(AK099XX_REG_WIA1, &wia1Value, AKMECOMPASS_LEN_ONE_BYTE)) != AkmECompass::SUCCESS) {
        return status;
    }

    // Checks the obtained value equals to the supposed value.
    if (wia1Value != AK099XX_VAL_WIA1) {
        return AkmECompass::ERROR;
    }

    // Gets the WIA2 register value.
    char wia2Value = 0;
    if ((status=AK099XX::read(AK099XX_REG_WIA2, &wia2Value, AKMECOMPASS_LEN_ONE_BYTE)) != AkmECompass::SUCCESS) {
        return status;
    }
    // Checks the obtained value equals to the supposed value.
    if ( wia2Value != deviceId ) {
        return AkmECompass::ERROR;
    }
    
    return status;
}

AkmECompass::Status AK099XX::isDataReady() {
    AkmECompass::Status status = AkmECompass::ERROR;
    
    // Gets the ST1 register value.
    char st1Value = 0;
    if ((status=AK099XX::read(AK099XX_REG_ST1, &st1Value, AKMECOMPASS_LEN_ONE_BYTE)) != AkmECompass::SUCCESS) {
        // I2C read failed.
        return status;
    }

    // Sets a return status corresponds to the obtained value.    
    if ((st1Value & AK099XX_BIT_MASK_DRDY) > 0) {
        status = AkmECompass::DATA_READY;
    } else {
        status = AkmECompass::NOT_DATA_READY;
    }    
    return status;
}

AkmECompass::Status AK099XX::getData(char *buf) {
    AkmECompass::Status status = AkmECompass::ERROR;
    
    if ((status=AK099XX::read(AK099XX_REG_ST1, buf, AK099XX_DATA_LENGTH)) != AkmECompass::SUCCESS) {
        // I2C read failed.
        return status;
    }
    return status;
}

AkmECompass::Status AK099XX::getOperationMode(AkmECompass::Mode *mode){
    AkmECompass::Status status = AkmECompass::ERROR;
    char buf = 0;
    
    if ((status=AK099XX::read(AK099XX_REG_CNTL2, &buf, AKMECOMPASS_LEN_ONE_BYTE)) != AkmECompass::SUCCESS) {
        // I2C read failed.
        return status;
    }
    mode->mode = (AkmECompass::OperationMode)(buf & AK099XX_BIT_MASK_MODE);
    
    if( deviceId == AK099XX::AK09912){
        buf = 0;
        if ((status=AK099XX::read(AK099XX_REG_CNTL1, &buf, AKMECOMPASS_LEN_ONE_BYTE)) != AkmECompass::SUCCESS) {
            // I2C write failed.
            return status;
        }
        mode->options[0] = (buf & AK099XX_BIT_MASK_NSF)>>AK099XX_BIT_OFFSET_NSF;
    }
    else if(deviceId == AK099XX::AK09915 || deviceId == AK099XX::AK09917 ){
        mode->options[1] = (buf & AK099XX_BIT_MASK_SDR)>>AK099XX_BIT_OFFSET_SDR;
        buf = 0;
        if ((status=AK099XX::read(AK099XX_REG_CNTL1, &buf, AKMECOMPASS_LEN_ONE_BYTE)) != AkmECompass::SUCCESS) {
            // I2C write failed.
            return status;
        }
        mode->options[0] = (buf & AK099XX_BIT_MASK_NSF)>>5;
    }
    return status; 
}

AkmECompass::Status AK099XX::setOperationMode(AkmECompass::Mode mode){
    AkmECompass::Status status = AkmECompass::ERROR;

    // The device has to be put into power-down mode first before switching mode.
    char buf = AkmECompass::MODE_POWER_DOWN;

    if(mode.mode == AkmECompass::MODE_SINGLE_LOOP)
    {
      // Switch to the specified mode
      buf = AkmECompass::MODE_SINGLE_MEASUREMENT;

      // SDR setting for AK09915/AK09917
      if(deviceId == AK099XX::AK09915 || deviceId == AK099XX::AK09917){
          // set SDR parameter
          buf |= mode.options[1]<<AK099XX_BIT_OFFSET_SDR & AK099XX_BIT_MASK_SDR;
      }

      if ((status=AK099XX::write(AK099XX_REG_CNTL2, &buf, AKMECOMPASS_LEN_ONE_BYTE)) != AkmECompass::SUCCESS) {
          // I2C write failed.
          return status;
      }
    }
    else
    {
      if (mode.mode != AkmECompass::MODE_POWER_DOWN) {
          if ((status=AK099XX::write(AK099XX_REG_CNTL2, &buf, AKMECOMPASS_LEN_ONE_BYTE)) != AkmECompass::SUCCESS) {
              // I2C write failed.
              return status;
          }
          wait_us(AK099XX_WAIT_POWER_DOWN_US);
      }

      // NSF setting for AK09912/AK09915/AK09917
      if(deviceId == AK099XX::AK09912 || deviceId == AK099XX::AK09915 || deviceId == AK099XX::AK09917){
          // set NSF parameter
          buf = (mode.options[0]<<AK099XX_BIT_OFFSET_NSF) & AK099XX_BIT_MASK_NSF;
          if ((status=AK099XX::write(AK099XX_REG_CNTL1, &buf, AKMECOMPASS_LEN_ONE_BYTE)) != AkmECompass::SUCCESS) {
              // I2C write failed.
              return status;
          }
      }

      // Switch to the specified mode
      buf = mode.mode;

      // SDR setting for AK09915/AK09917
      if(deviceId == AK099XX::AK09915 || deviceId == AK099XX::AK09917){
          // set SDR parameter
          buf |= mode.options[1]<<AK099XX_BIT_OFFSET_SDR & AK099XX_BIT_MASK_SDR;
      }

      if ((status=AK099XX::write(AK099XX_REG_CNTL2, &buf, AKMECOMPASS_LEN_ONE_BYTE)) != AkmECompass::SUCCESS) {
          // I2C write failed.
          return status;
      }
//      wait_us(AK099XX_WAIT_POWER_DOWN_US);
    }
    return AkmECompass::SUCCESS;
}

AkmECompass::Status AK099XX::getMagneticVectorLsb(MagneticVectorLsb *lsb) {
    AkmECompass::Status status = AkmECompass::ERROR;
    char buf[AK099XX_DATA_LENGTH];
    
    if ((status=AK099XX::getData(buf)) != AkmECompass::SUCCESS) {
        // Failed to get data.
        return status;
    }
    
    // Checks sensor overflow
    if ( (buf[8] & AK099XX_BIT_MASK_HOFL) > 0 ) {
        // Magnetic sensor overflow
        lsb->isOverflow = true;
    } else {
        // Magnetic sensor didn't overflow.
        lsb->isOverflow = false;
        // Calculates magnetic field value
        int16_t xi;
        int16_t yi;
        int16_t zi;
        if(deviceId == AK099XX::AK09917){
            xi = (int16_t)( buf[1]<<8 | buf[2] );
            yi = (int16_t)( buf[3]<<8 | buf[4] );
            zi = (int16_t)( buf[5]<<8 | buf[6] );                        
        } else {
            xi = (int16_t)( buf[2]<<8 | buf[1] );
            yi = (int16_t)( buf[4]<<8 | buf[3] );
            zi = (int16_t)( buf[6]<<8 | buf[5] );            
        }
        lsb->lsbX = (int32_t)( xi * ((sensitivityAdjustment.x + 128)/denominator) );
        lsb->lsbY = (int32_t)( yi * ((sensitivityAdjustment.y + 128)/denominator) );
        lsb->lsbZ = (int32_t)( zi * ((sensitivityAdjustment.z + 128)/denominator) );
        lsb->lsbTemp = (int8_t)buf[7];
    }
    return AkmECompass::SUCCESS;
}

AkmECompass::Status AK099XX::getMagneticVector(MagneticVector *vec) {
    AkmECompass::Status status = AkmECompass::ERROR;
    MagneticVectorLsb lsb;
    status = getMagneticVectorLsb(&lsb);
    if(status == AkmECompass::SUCCESS){
        vec->isOverflow = lsb.isOverflow;
        vec->mx = (float)( lsb.lsbX * sensitibity );
        vec->my = (float)( lsb.lsbY * sensitibity );
        vec->mz = (float)( lsb.lsbZ * sensitibity );
        if(deviceId == AK099XX::AK09912){
            vec->temp = (float)( 35.0 + (120.0 - lsb.lsbTemp)/1.6 );
        }
        else{
            vec->temp = 0.0;    
        }
    }else{
        return status;
    }
    return AkmECompass::SUCCESS;
}


AkmECompass::Status AK099XX::getSensitivityAdjustment(SensitivityAdjustment *sns) {
    AK099XX::Status status = AK099XX::ERROR;
    
    // No need to access the Fused-ROM for these sensors
    if( !isRequiredAsa ){
        sns->x = 128;
        sns->y = 128;
        sns->z = 128;
        return AK099XX::SUCCESS;
    }
    
    // Set the device into the fuse ROM access mode.
    char data = AK099XX_MODE_FUSE_ACCESS;
    if ((status=AK099XX::write(AK099XX_REG_CNTL2, &data, AKMECOMPASS_LEN_ONE_BYTE)) != AkmECompass::SUCCESS) {
        // I2C write failed.
        return status;
    }
    
    // Wait
    wait_us(AK099XX_WAIT_POWER_DOWN_US);
    
    // Read the fuse ROM
    char buf[AK099XX_BUF_LENGTH_ASA];
    if ((status=AK099XX::read(AK099XX_REG_ASAX, buf, AK099XX_BUF_LENGTH_ASA)) != AkmECompass::SUCCESS) {
        // I2C read failed.
        return status;
    }
    
    sns->x = buf[0];
    sns->y = buf[1];
    sns->z = buf[2];
    
    // Set the device into the power-down mode
    data = AK099XX_MODE_POWERDOWN;
    if ((status=AK099XX::write(AK099XX_REG_CNTL2, &data, AKMECOMPASS_LEN_ONE_BYTE)) != AkmECompass::SUCCESS) {
        // I2C write failed.
        return status;
    }
    
    // Wait
    wait_us(AK099XX_WAIT_POWER_DOWN_US);
    
    return AkmECompass::SUCCESS;  
}
