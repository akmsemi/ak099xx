#ifndef AK099XX_H
#define AK099XX_H

#include "mbed.h"
#include "akmecompass.h"

/**
 * This is a device driver of AK099XX 3-axis magnetometer.
 *
 * @note AK0991X is a 3-axis magnetometer (magnetic sensor) device manufactured by AKM.
 * @note Available sensors: AK09911C/AK09912C/AK09915C/AK09915D/AK09916C/AK09916D
 * @note No FIFO support for AK09915C/AK09915D
 * 
 * Example:
 * @code
 * #include "mbed.h"
 * #include "ak099XX.h"
 * 
 * #define I2C_SPEED_100KHZ    100000
 * #define I2C_SPEED_400KHZ    400000
 * 
 * int main() {
 *     // Creates an instance of I2C
 *     I2C connection(I2C_SDA0, I2C_SCL0);
 *     connection.frequency(I2C_SPEED_100KHZ);
 *
 *     // Creates an instance of AK099XX
 *     AK099XX ak099xx(&connection, AK099XX::SLAVE_ADDR_1);
 *     
 *     // Checks connectivity
 *     if(ak099xx.checkConnection() != AK099XX::SUCCESS) {
 *         // Failed to check device connection.
 *         // - error handling here -
 *     }
 *     
 *     // Puts the device into continuous measurement mode.
 *     AkmECompass::Mode mode;
 *     mode.mode = AK099XX::MODE_CONTINUOUS_1;
 *     if(ak099xx.setOperationMode(mode) != AK099XX::SUCCESS) {
 *         // Failed to set the device into continuous measurement mode.
 *         // - error handling here -
 *     }
 *
 *     while(true) {
 *         // checks DRDY
 *         if (statusAK099XX == AK099XX::DATA_READY) {
 *             AK099XX::MagneticVector mag;
 *             ak099xx.getMagneticVector(&mag);
 *             // You may use serial output to see data.
 *             // serial.printf("%d,%5.1f,%5.1f,%5.1f\n",
 *             //    mag.isOverflow,
 *             //    mag.mx, mag.my, mag.mz);
 *             statusAK099XX = AK099XX::NOT_DATA_READY;
 *         } else if (statusAK099XX == AK099XX::NOT_DATA_READY) {
 *             // Nothing to do.
 *         } else {
 *             // - error handling here -
 *         }
 *     }
 * }
 * @endcode
 */
class AK099XX : public AkmECompass
{
public:
    
    /**
     * Constructor.
     *
     */
    AK099XX();

    /**
     * Destructor.
     *
     */
    virtual ~AK099XX();
    
    /**
     * Initialize sensor with I2C connection.
     *
     * @param _i2c Pointer to the I2C instance
     * @param addr Slave address of the device
     * @param deviceId Device Id enum
     *
     */
    virtual void init(I2C *_i2c, SlaveAddress addr, DeviceId deviceId);

    /**
     * Initialize sensor with SPI connection.
     *
     * @param _spi Pointer to the SPI instance
     * @param _cs Pointer to the chip select DigitalOut pin
     * @param deviceId Device Id enum
     *
     */
    virtual void init(SPI *_spi, DigitalOut *_cs, DeviceId deviceId);
    
    /**
     * Check the connection. 
     *
     * @note Connection check is performed by reading a register which has a fixed value and verify it.
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    virtual AkmECompass::Status checkConnection();

    /**
     * Gets device operation mode.
     *
     * @param mode device opration mode
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    virtual AkmECompass::Status getOperationMode(Mode *mode);

    /**
     * Sets device operation mode.
     *
     * @param mode device opration mode
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    virtual AkmECompass::Status setOperationMode(Mode mode);

    /**
     * Check if data is ready, i.e. measurement is finished.
     *
     * @return Returns DATA_READY if data is ready or NOT_DATA_READY if data is not ready. If error happens, returns another code.
     */
    virtual AkmECompass::Status isDataReady();

    /**
     * Gets magnetic vector from the device in LSB.
     *
     * @param lsb Pointer to a instance of MagneticVectorLsb
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    virtual AkmECompass::Status getMagneticVectorLsb(MagneticVectorLsb *lsb);

    /**
     * Gets magnetic vector from the device in uT.
     *
     * @param vec Pointer to a instance of MagneticVector
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    virtual AkmECompass::Status getMagneticVector(MagneticVector *vec);

private:
            
    /**
     * Holds sensitivity adjustment values
     */
    SensitivityAdjustment sensitivityAdjustment;    

    /**
     * Holds the sensitivity of the sensor.
     */
    float sensitibity;
    
    /**
     * Holds the denominator used for calculating the mag value from the register value.
     */
    float denominator;
        
    /**
     * Holds if it is required to read ASA value from the fused ROM
     */
    bool isRequiredAsa;
    
    /**
     * Gets magnetic data, from the register ST1 (0x02) to ST2 (0x09), from the device.
     * 
     * @param buf buffer to store the data read from the device.
     * 
     * @return Returns SUCCESS when succeeded, otherwise returns another.
     */
    AkmECompass::Status getData(char *buf);
    
    /**
     * Gets sensitivity adjustment values from the register ASAX, ASAY, and ASAZ.
     *
     * @param buf buffer to store sensitivity adjustment data
     *
     * @return SUCCESS when succeeded, otherwise returns another.
     */
    AkmECompass::Status getSensitivityAdjustment(SensitivityAdjustment *sns);
    
    
    /**
     * Sub init function.
     *
     */
    void init_sub();
};

#endif
